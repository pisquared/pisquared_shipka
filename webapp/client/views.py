from collections import OrderedDict, defaultdict

from flask import render_template, flash, redirect, request
from flask_security import login_required, roles_required

from webapp.client import client
from webapp.api.models import BlogPost


class OrderedDefaultDict(OrderedDict, defaultdict):
    def __init__(self, default_factory=None, *args, **kwargs):
        super(OrderedDefaultDict, self).__init__(*args, **kwargs)
        self.default_factory = default_factory


@client.route('/')
def get_blog():
    blog_posts = BlogPost.get_all_by(order_by=BlogPost.created_ts.desc())
    dated_bp = OrderedDefaultDict(list)
    for bp in blog_posts:
        dated_bp[bp.created_ts.date()].append(bp)
    return render_template('index.html',
                           dated_bp=dated_bp,
                           title="PiSquared Blog")


@client.route('/projects')
def get_projects():
    return render_template('projects.html',
                           title="PiSquared Projects")

@client.route('/about_me')
def get_aboutme():
    return render_template('aboutme.html',
                           title="PiSquared About Me")


@client.route('/blog_posts/new')
@login_required
@roles_required('admin',)
def get_blog_posts_new():
    return render_template('blog_posts_new.html',
                           title="PiSquared Projects")


@client.route('/blog_posts/<int:instance_id>/edit')
@login_required
@roles_required('admin', )
def get_blog_posts_edit(instance_id):
    bp = BlogPost.get_one_by(id=instance_id)
    if not bp:
        flash('No BlogPost with id {}'.format(instance_id))
        return redirect(request.referrer)
    return render_template('blog_posts_edit.html',
                           title="PiSquared Projects",
                           bp=bp)
