from shipka.store.search import whooshee

from shipka.store.database import db, ModelController, Datable, Deletable, Ownable


@whooshee.register_model('body')
class BlogPost(db.Model, ModelController, Datable, Deletable, Ownable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)
