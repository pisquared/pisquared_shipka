from shipka.webapp.util import ModelView
from webapp.api import api
from webapp.api.models import BlogPost

MODELS_REGISTRY = {}

MODELS_REGISTRY.update({
    'blog_post': {
        'vm': ModelView,
        'model': BlogPost,
        'creatable_roles_required': ['admin', ],
        'editable_roles_required': ['admin', ],
        'deletable_roles_required': ['admin', ],
    },
})

# Populate MODELS_REGISTRY with extra metadata
for model_name, registry in MODELS_REGISTRY.iteritems():
    MODELS_REGISTRY[model_name]['name'] = model_name
    VMClass = MODELS_REGISTRY[model_name]['vm']
    vm = VMClass(
        name=model_name,
        model=registry['model'],
        form_include=registry.get('form_include', []),
        form_exclude=registry.get('form_exclude', []),
        form_only=registry.get('form_only', []),
        retrievable_login_required=registry.get('retrievable_login_required', True),
        retrievable_roles_required=registry.get('retrievable_roles_required', []),
        creatable=registry.get('creatable', True),
        creatable_login_required=registry.get('creatable_login_required', True),
        creatable_roles_required=registry.get('creatable_roles_required', []),
        editable=registry.get('editable', True),
        editable_login_required=registry.get('editable_login_required', True),
        editable_roles_required=registry.get('editable_roles_required', []),
        deletable=registry.get('deletable', True),
        deletable_login_required=registry.get('deletable_login_required', True),
        deletable_roles_required=registry.get('deletable_roles_required', []),
        user_needed=registry.get('user_needed', True),
    )
    MODELS_REGISTRY[model_name]['vm'] = vm
    vm.register(api)
