#!/usr/bin/env bash
HELP="
Usage $0 [ assets | admin_webapp | beat | bootstrap | celery | db | dev | eventlet | flower | journal | pgweb | populate | prod_bootstrap | prod_push | recreate | reprov_project | runserver | shell | status | sys | test | test_prod | translate | vm ]

    DEV ACTIVITIES:
    ===============
    bootstrap           Bootstrap the project by cloning shipka repo
    populate            Populates a database with initial values.
    recreate            Recreates everything - db, workers etc.
    reprov_project      Run the project specific provisioners only.
    test                Run BDD tests with lettuce
    test_prod           Setup a local prod machine for testing purposes
                          EXCEPT:
                             - setting up an SSL certificate
    translate           Generates translation files.

    ADMIN ACTIVITIES:
    ===============
    **!!Only if you have configured sensitive.py and have sensitive folder!!**
    prod_bootstrap      TODO Bootstrap Production - designed for bootstrapping before ansible can kick in. Works on Digital Ocean
    prod_push           Push to production:

    SERVICES:
    ===============
    sys                 alias to sudo systemctl #@
    journal             alias to sudo journalctl -u #@
    vm                  alias to vagrant #@
    runall              Run default development environment - server+worker+cron.
    runserver           Runs the server.
        --debug-fe=True      Enable debugging of front end by not compressing of static files.
        --debug-tb=True      Enable debug toolbar.
        --ssl=True           Enable running with certificates.
        prod                 Run in production.
    eventlet            Run eventlet server.
    celery              Run celery worker.
    beat                Run beat scheduler.

    ADMIN SERVICES:
    ===============
    admin_webapp        Run admin webapp.
    flower              Run flower management.
    pgweb               Run database management.

    MANAGEMENT:
    ===============
    assets              Manage static assets.
    db                  Perform database migrations.

    DEBUGING:
    ===============
    status              View the status of the installed and running services
    shell               Runs a Python shell inside Flask application context.
"

HELP_TRANSLATION="
USAGE ./manage.sh translate [extract|gen {lang}|compile|update]

    extract             Extract strings in files as defined in translations/babel.cfg
    gen {lang}          Init translations for {lang}
    compile             Compile all translations
    update              Use after a new extract - it may mark strings as fuzzy.
"

if [ ! -f local_config.json ]; then
   echo "Path to clone/existing shipka repo (default: $(realpath $(pwd)/../shipka)): "
   read -r SHIPKA_PATH
   if [[ -z "$SHIPKA_PATH" ]]; then
        SHIPKA_PATH=$(realpath $(pwd)/../shipka)
   fi
   python -c "import json;json.dump({'local_shipka_path': '$SHIPKA_PATH'}, open('local_config.json', 'w+'))"
else
  SHIPKA_PATH=`python -c 'import json;j=json.load(open("local_config.json"));print j["local_shipka_path"]'`
fi
SHIPKA_REMOTE_PATH=`python -c 'import json;j=json.load(open("project_config.json"));print j["remote_shipka_path"]'`

bootstrap() {
    [ ! -d "${SHIPKA_PATH}" ] && git clone https://gitlab.com/pisquared/shipka "${SHIPKA_PATH}"
    cp sensitive_config.json.dev sensitive_config.json
    VAGRANT_VERSION_EXPECTED="Vagrant 2.0.2"
    VAGRANT_VERSION=`vagrant --version`

    if [ ! "$VAGRANT_VERSION" == "$VAGRANT_VERSION_EXPECTED" ]; then
        echo "WARN: Vagrant version different than expected. It may still work but if there are any problems - Please install $VAGRANT_VERSION_EXPECTED - https://www.vagrantup.com/downloads.html"
    fi
}

command_db() {
    DB_COMMAND=$2
    case "$DB_COMMAND" in
        migrate) "${SHIPKA_PATH}/scripts/db_migrate.sh" "$@"
        ;;
        *) ./_manage.py "$@"
        ;;
    esac
    return $?
}

command_translate() {
    TRANSLATE_COMMAND=$2
    shift
    shift
    case "$TRANSLATE_COMMAND" in
        extract) pybabel extract -F translations/babel.cfg -o translations/messages.pot .
        ;;
        gen) pybabel init -i translations/messages.pot -d translations -l "$@"
        ;;
        compile) pybabel compile -d translations
        ;;
        update) pybabel update -i translations/messages.pot -d translations
        ;;
        *)  >&2 echo -e "${HELP_TRANSLATION}"
        ;;
    esac
    return $?
}

command_main() {
    INITIAL_COMMAND=$1
    case "$INITIAL_COMMAND" in
        assets|shell|runserver|runadmin|populate) ./_manage.py "$@"
        ;;
        admin_webapp) "${SHIPKA_REMOTE_PATH}/scripts/run_admin_webapp.sh" "$@"
        ;;
        runall) sudo systemctl stop celery beat && ./_manage.py "$@"
        ;;
        beat) "${SHIPKA_REMOTE_PATH}/scripts/run_beat.sh" "$@"
        ;;
        bootstrap) bootstrap
        ;;
        celery) "${SHIPKA_REMOTE_PATH}/scripts/run_celery.sh" "$@"
        ;;
        db) command_db "$@"
        ;;
        eventlet) "${SHIPKA_REMOTE_PATH}/scripts/run_eventlet.sh" "$@"
        ;;
        journal) shift; sudo journalctl -u "$@"
        ;;
        flower) "${SHIPKA_REMOTE_PATH}/scripts/run_flower.sh" "$@"
        ;;
        pgweb) "${SHIPKA_REMOTE_PATH}/scripts/run_pgweb.sh" "$@"
        ;;
        prod_bootstrap) "${SHIPKA_REMOTE_PATH}scripts/prod_bootstrap.sh" "$@"
        ;;
        prod_push) "${SHIPKA_REMOTE_PATH}/scripts/prod_push.sh" "$@"
        ;;
        recreate) "${SHIPKA_REMOTE_PATH}/scripts/recreate.sh"
        ;;
        reprov_project) vagrant provision --provision-with project_specific
        ;;
        status) shift && "${SHIPKA_REMOTE_PATH}/scripts/status.py" "$@"
        ;;
        sys) shift; sudo systemctl "$@"
        ;;
        test) lettuce
        ;;
        test_prod) "${SHIPKA_PATH}/scripts/test_prod.sh"
        ;;
        translate) command_translate "$@"
        ;;
        vm) shift; vagrant "$@"
        ;;
        *) >&2 echo -e "${HELP}"
        return 1
        ;;
    esac
    return $?
}

command_main "$@"
