#!/usr/bin/env python
"""
Used for easy integration with python-based IDEs like PyCharm.
"""

import sys

try:
    import eventlet

    if sys.argv[1] != 'shell':
        eventlet.sleep()
        eventlet.monkey_patch()
except ImportError:
    # This would fail if websockets are not enabled but it's too early to check for that
    print("INFO: eventlet monkey patching failed. It's OK if websockets are not enabled.")

if __name__ == '__main__':
    from shipka.manager import manager

    manager.run()
