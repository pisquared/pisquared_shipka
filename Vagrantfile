# -*- mode: ruby -*-
# vi: set ft=ruby :
require 'json'
require 'fileutils'

if not File.exist?("sensitive_config.json")
  FileUtils.cp("sensitive_config.json.dev", "sensitive_config.json")
end

if not File.exist?("local_config.json")
  FileUtils.cp("local_config.json.dev", "local_config.json")
end

local_config_f = File.read('local_config.json')
local_config = JSON.parse(local_config_f)

project_config_f = File.read('project_config.json')
project_config = JSON.parse(project_config_f)

sensitive_config_f = File.read('sensitive_config.json')
sensitive_config = JSON.parse(sensitive_config_f)

all_config = local_config.merge(project_config)
all_config = all_config.merge(sensitive_config)
all_config["project_root"] = "%{core_dir}/%{app_name}" % { :core_dir => all_config["core_dir"], :app_name => all_config["app_name"] }
all_config["project_venv"] = "%{core_dir}/venv" % { :core_dir => all_config["core_dir"]}

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"         # Ubuntu 16.04.3
  config.vm.box_version = "20171118.0.0"    # 4.4.0-101-generic Nov 10 2017

  config.vm.provider "virtualbox" do |vb|
     vb.memory = "2048"
  end

  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"

  # Configure ansible local provisioner
  config.vm.provision "ansible_local" do |ansible|
    ansible.install_mode = "pip"
    ansible.version = "2.3.0.0"
    ansible.verbose = "v"
    ansible.provisioning_path = all_config["remote_shipka_path"]
    ansible.playbook = "provisioning/playbook.yml"

    ansible.extra_vars = all_config
  end

  config.vm.provision "project_specific", type: "ansible_local" do |ansible|
    ansible.install_mode = "pip"
    ansible.version = "2.3.0.0"
    ansible.verbose = "v"
    ansible.provisioning_path = all_config["project_root"]
    ansible.playbook = "provisioning/playbook.yml"

    ansible.extra_vars = all_config
  end

  # Test Prod Machine
  config.vm.define :test_prod, autostart: false do |test_prod|
    test_prod.vm.network "private_network", ip: "192.168.50.100"
    test_prod.vm.network "forwarded_port", guest: 80, host: 8888
    test_prod.vm.synced_folder ".", "/vagrant"
    test_prod.ssh.forward_agent = true

    test_prod.vm.provision "test_prod", type: "ansible" do |ansible|
      ansible.playbook = all_config["local_shipka_path"] + "/provisioning/playbook.yml"
      ansible.groups = {
        "webservers" => ["test_prod"],
        "dbservers" => ["test_prod"],
      }

      all_config["ansible_ssh_user"] = "vagrant"
      all_config["ssl_enabled"] = false
      all_config["cwd"] = Dir.pwd
      ansible.extra_vars = all_config
    end

    test_prod.vm.provision "test_prod_specific", type: "ansible" do |ansible|
      ansible.playbook = "provisioning/playbook.yml"
      ansible.groups = {
        "webservers" => ["test_prod"],
        "dbservers" => ["test_prod"],
      }

      all_config["ansible_ssh_user"] = "vagrant"
      all_config["ssl_enabled"] = false
      all_config["cwd"] = Dir.pwd
      ansible.extra_vars = all_config
    end
  end

  # Development machine
  config.vm.define :dev, primary: true do |dev|
    dev.ssh.username = "vagrant"
    dev.vm.synced_folder ".", all_config["project_root"]
    dev.vm.synced_folder all_config["local_shipka_path"], all_config["remote_shipka_path"]
    # dev.vm.synced_folder "webapp/static", "/var/www/#{APP_NAME}/webapp/static", :owner => "www-data"

    # dev webapp
    dev.vm.network "forwarded_port", guest: all_config["app_port"], host: all_config["app_port"]

    # admin webapp
    if all_config["admin_webapp_enabled"]
        dev.vm.network "forwarded_port", guest: all_config["admin_webapp_port"], host: all_config["admin_webapp_port"]
    end

    # pgweb
    if all_config["database_mgmt_enabled"]
        dev.vm.network "forwarded_port", guest: all_config["database_mgmt_port"], host: all_config["database_mgmt_port"]
    end

    # rabbitmq-admin
    if all_config["queue_mgmt_enabled"]
        dev.vm.network "forwarded_port", guest: 15672, host: all_config["queue_mgmt_port"]
    end

    # flower
    if all_config["workers_mgmt_enabled"]
        dev.vm.network "forwarded_port", guest: all_config["workers_mgmt_port"], host: all_config["workers_mgmt_port"]
    end

    # prod webapp proxied
    if all_config["webapp_proxy_enabled"]
        dev.vm.network "forwarded_port", guest: 80, host: all_config["webapp_proxy_http_port"]
    end

    # monitoring
    if all_config["monitoring_mgmt_enabled"]
        dev.vm.network "forwarded_port", guest: all_config["monitoring_mgmt_port"], host: all_config["monitoring_mgmt_port"]
    end

    # error tracking
    if all_config["error_tracking_enabled"]
        dev.vm.network "forwarded_port", guest: all_config["error_tracking_port"], host: all_config["error_tracking_port"]
    end
  end
end
